# Corso di formazione Excel Ottobre 2021  #

Corso di formazione sull'utilizzo di Excel dal livello **base** a quello **avanzato**.

Di seguito la struttura del corso. Ogni blocco corrisponde a 2 ore di lezione frontale. 

### Livello Base (2h) ###

* Conoscenza Excel 
* Struttura del Foglio
* Formattazione Celle

### Livello Intermedio (6h) ###

* Tabelle
* Ordinamento
* Filtri sui dati

=================================

* Riferimenti a Celle
* Gestione Nomi
* Operazioni Matematiche
* Funzioni Logiche e di Contaggio

=================================

* Funzioni Avanzato

### Livello Avanzato (4h)

* Funzioni di Ricerca

=================================

* Tabelle Pivot

